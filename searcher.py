import glob
import os

from file_compare_serial import FileCompareSerial

# TODO: break out responsibilities
# 1. List of files finder (how to navigate file structure,
# 2. Find the hash of the file
# 3. Collect list of duplicates
from file_finder import FileFinder


class Searcher:
    # TODO: add estimation method
    @staticmethod
    def search(path, ext, search_recursive=True):
        if not os.path.isdir(path):
            raise CanNotFindDirectory(path)

        file_hash = dict()
        dup_keys = list()

        for fullname in FileFinder.find_files_by_extension(path=path, ext=ext, search_recursive=search_recursive):
            files_hash = FileCompareSerial.md5(fullname)
            if files_hash not in file_hash.keys():
                new_list = [fullname, ]
                file_hash[files_hash] = new_list
                pass
            else:
                (file_hash[files_hash]).append(fullname)
                dup_keys.append(files_hash)
                pass

        return file_hash, dup_keys


class CanNotFindDirectory(ValueError):
    pass
