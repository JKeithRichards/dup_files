# Program Design
The program should be able to run without the GUI. 
The GUI should just be a pretty frontend. 
main.py is aware of the GUI

## Creating/Updating the GUI
* "Draw" the GUI in the Qt Creator, then save as gui.py. 
* Update main.py with any new or modified variables in the gui
* Convert gui to python code: `pyuic5 gui.ui -o gui.py`
* Package code into one file: `pyinstaller --onefile main.py`

## Program flow
1. main.py
    1. Create a thread Worker wrapper around Searcher
    2. Starts GUI
    3. "Listen" for button clicks
        1. `File Browser` - Open a file browser dialog, put the result in the 
        `le_search_folder`
        2. `Search` 
            1. Check a search directory has been selected
            2. Build a list of file extensions to search for
            3. Thread house keeping
            4. Start the worker thread wrapped around the Searcher
            5. Update the `tb_results` when Searcher returns results
2. gui.py displays window. Buttons link back to functions in `main.py`
3. `main.py` -> `search()`
4. Searcher: Call `FileFinder` to get a list of files to check for duplicates
    1. FileFinder: `yield` a list of all files matching the requested file extensions
    1. Hash the file
    1. Check if hash in list of file hashes: 
    Note: `files_hash` is a dict of lists.
        1. If it is not, add it to the dict, hash is key, file name appended
        to the list that is the value
        1. If it is, add the hash to the list of duplicate keys
    1. Pass back a list of duplicate keys, and the dict with the dup file names
    

## Misc Notes:
* Using Qt Designer: http://pyqt.sourceforge.net/Docs/PyQt5/designer.html
* Install pyuic5: sudo apt install pyqt5-dev-tools
* Install pyinstaller
    * `apt-get install upx-ucl`
    * `apt-get install python3.6-dev`
    * `pip install pyinstaller`

## Future features (in no particular order):
* Text listing duplicates have hyperlinks that open explorer to the image/file
* Able to cancel an in-progress search
* Progress bar
* Split out freemium features
* thumbnail next to files listed as duplicates
* "similar" hashes
* Add a "close" button to the GUI
* update the results box as
    * duplicates are found
    * new subdirectories are found to search 
* Remember directory selected last time program was used
* remove `dup_keys` list, just pull data from dict.keys()
* Save dup file list as text file
* Check files declared mime type instead of relying on file extensions
