#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This files present the GUI to the users, then extracts data from the GUI and
# calls the file_compare with the appropriate parameters.
import json
import os
import sys
import time

from PyQt5 import QtWidgets
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, QThread

import gui
from searcher import Searcher, CanNotFindDirectory


# A Thread wrapper around the Searcher class so the the GUI can stay responsive
# while the Searcher finds and compares files.
class Worker(QObject):
    finished = pyqtSignal()
    data_ready = pyqtSignal(dict, list)

    def init(self, path, ext, recursive):
        """
        A slot takes no parameters, so this functions sets up the variables
        the worker needs to pass to the Searcher.
        """
        self.path = path
        self.ext = ext
        self.recursive = recursive

    @pyqtSlot()
    def search_it(self):  # A slot takes no params
        file_hash, dup_keys = Searcher.search(
            path=self.path, ext=self.ext, search_recursive=self.recursive)
        self.data_ready.emit(file_hash, dup_keys)
        self.finished.emit()


class GuiApp(QtWidgets.QMainWindow, gui.Ui_MainWindow):
    CONFIG_FILE_BASENAME = 'config.json'

    def __init__(self, parent=None):
        super(GuiApp, self).__init__(parent)
        self.setupUi(self)
        self.btn_browse.clicked.connect(self.browse_folder)
        self.btn_search.clicked.connect(self.search)
        self.start_time = 0
        self.config = dict()
        self.cur_dir = os.path.dirname(os.path.realpath(__file__))
        self.config_full_path_name = os.path.join(self.cur_dir, self.CONFIG_FILE_BASENAME)
        if os.path.exists(self.config_full_path_name):
            with open(self.config_full_path_name) as config_data:
                self.config = json.load(config_data)

    def browse_folder(self):

        self.le_search_folder.setText(QtWidgets.QFileDialog.getExistingDirectory(
            self,
            "Select directory to search for duplicate files",
            self.config.get('search_dir', os.path.dirname(os.path.realpath(__file__))),
            QtWidgets.QFileDialog.ShowDirsOnly
            | QtWidgets.QFileDialog.DontResolveSymlinks
            | QtWidgets.QFileDialog.DontUseNativeDialog
        ))
        self.config['search_dir'] = self.le_search_folder.text()
        with open(self.config_full_path_name, 'w') as conf_file:
            json.dump(self.config, conf_file)

    def search(self):
        """
        Gather information from widgets state in the GUI, use the data to set
        up a Thread class worker.
        :return:
        """
        try:
            path = self.le_search_folder.text()
            if not path:
                raise EmptySearchDir

            # Build a list of extensions to search for by checking which
            # extension boxes the user checked on the GUI.
            ext = []
            if self.cb_image_ext.isChecked():
                ext += self.lb_image_ext.text().split(",")
            if self.cb_doc_ext.isChecked():
                ext += self.lb_doc_ext.text().split(",")
            if self.cb_custom_ext.isChecked():
                ext += self.le_custom_ext.text().split(",")
            if self.cb_audo_ext.isChecked():
                ext += self.lb_audio_ext.text().split(",")

            if not ext:
                raise NoExtSelected

            # False: Only search the current directory, don't search
            # subdirectories.
            r = False
            if self.cb_recursive.isChecked():
                r = True

            # Remove any text from previous searches
            self.tb_results.clear()

            self.obj = Worker()
            self.thread = QThread()

            self.obj.data_ready.connect(self.on_finished)
            self.obj.moveToThread(self.thread)
            self.obj.finished.connect(self.thread.quit)
            self.thread.started.connect(self.obj.search_it)

            self.tb_results.append("Searching, please wait...")
            self.start_time = time.time()
            self.obj.init(path, ext, r)
            self.thread.start()

        except NoExtSelected:
            self.popup_error_message(
                "No file extensions selected!",
                "Please select some file extensions types you want searched for duplicates."
            )
        except EmptySearchDir:
            self.popup_error_message(
                "No search directory selected!",
                "Please specify a directory to start searching for duplicate files."
            )
        except CanNotFindDirectory:
            self.popup_error_message(
                "Invalid directory!",
                "Unable to find the directory '{}'".format(path)
            )

    def on_finished(self, file_hash, dup_keys):
        tot_time = time.time() - self.start_time
        self.tb_results.setText("Searched {} files in {} seconds.".format(len(file_hash), round(tot_time, 2)))
        self.tb_results.append("Found {} set of duplicate files.".format(len(dup_keys)))
        if dup_keys:
            self.tb_results.append("Duplicate files:")
            for i, key in enumerate(dup_keys):
                self.tb_results.append("{} duplicate set:".format(i + 1))
                for img in file_hash[key]:
                    self.tb_results.append("    {}".format(img))

    def popup_error_message(self, title, message):
        choice = QtWidgets.QMessageBox.question(
            self,
            title,
            message,
            QtWidgets.QMessageBox.Ok
        )


class NoExtSelected(Exception):
    pass


class EmptySearchDir(Exception):
    pass


def main():
    app = QtWidgets.QApplication(sys.argv)
    form = GuiApp()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
