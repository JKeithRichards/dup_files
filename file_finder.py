import glob
import os


class FileFinder:
    @staticmethod
    def find_files_by_extension(path, ext, search_recursive=True):
        search_path = os.path.join(path, '**')
        for fullname in glob.iglob(search_path, recursive=search_recursive):
            name, file_extension = os.path.splitext(fullname)
            if file_extension[1:].lower() in ext:
                yield fullname
